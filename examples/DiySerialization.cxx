//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//============================================================================

#include <vtkm/cont/ArrayCopy.h>
#include <vtkm/cont/ArrayGetValues.h>
#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/ArrayHandleCounting.h>
#include <vtkm/cont/DIYMemoryManagement.h>
#include <vtkm/cont/EnvironmentTracker.h>
#include <vtkm/cont/Initialize.h>
#include <vtkm/cont/testing/Testing.h>
#include <vtkm/thirdparty/diy/diy.h>

#include <iostream>
#include <memory>

const int TEST_NUM_POINTS = 5;

struct TimedCoords;
TimedCoords ComputeLocalCoords(int gid);
bool operator!=(const TimedCoords& a, const TimedCoords& b);

//// BEGIN-EXAMPLE DIYSerialization.cxx
struct TimedCoords
{
  vtkm::cont::ArrayHandle<vtkm::UInt64> TimeStamps;
  vtkm::cont::ArrayHandle<vtkm::Vec3i> Coordinates;
};

namespace vtkmdiy
{
template<>
struct Serialization<TimedCoords>
{
  static void save(BinaryBuffer& bb, const TimedCoords& p)
  {
    vtkmdiy::save(bb, p.TimeStamps);
    vtkmdiy::save(bb, p.Coordinates);
  }
  static void load(BinaryBuffer& bb, TimedCoords& p)
  {
    vtkmdiy::load(bb, p.TimeStamps);
    vtkmdiy::load(bb, p.Coordinates);
  }
};
}

void compute(vtkmdiy::Master& master, vtkmdiy::Link* link, int gid)
{
  TimedCoords timedCoords;
  master.add(gid, &timedCoords, link);

  master.foreach (
    [&](TimedCoords* tc, const vtkmdiy::Master::ProxyWithLink& cp)
    {
      *tc = ComputeLocalCoords(gid);
      cp.enqueue(cp.link()->target(0), *tc);
    });
  vtkm::cont::DIYMasterExchange(master);
  master.foreach (
    [&](TimedCoords* tc, const vtkmdiy::Master::ProxyWithLink& cp)
    {
      cp.dequeue(cp.link()->target(0).gid, *tc);
      auto expectedVec = ComputeLocalCoords(gid);
      if (*tc != expectedVec)
      {
        std::cerr << "ERROR: recieved incorrect vec values." << std::endl;
        //// PAUSE-EXAMPLE
        VTKM_TEST_FAIL("Received incorrect vec values.");
        //// RESUME-EXAMPLE
      }
    });
}
//// END-EXAMPLE DIYSerialization.cxx

TimedCoords ComputeLocalCoords(int gid)
{
  auto timeStamps =
    vtkm::cont::make_ArrayHandleCounting<vtkm::UInt64>(0, 1, TEST_NUM_POINTS);
  auto coordinates = vtkm::cont::make_ArrayHandleCounting<vtkm::Vec3i>(
    vtkm::make_Vec(0, 0, 0), vtkm::make_Vec(1, gid, 0), TEST_NUM_POINTS);

  // This depends on RTV optimization
  TimedCoords output;
  vtkm::cont::ArrayCopy(timeStamps, output.TimeStamps);
  vtkm::cont::ArrayCopy(coordinates, output.Coordinates);
  return output;
}

bool operator!=(const TimedCoords& a, const TimedCoords& b)
{
  if (!a.TimeStamps.GetNumberOfValues() || !b.TimeStamps.GetNumberOfValues())
  {
    VTKM_TEST_FAIL("Received incorrect vec values.");
    return true;
  }
  if (a.TimeStamps.GetNumberOfValues() != b.TimeStamps.GetNumberOfValues() ||
      a.Coordinates.GetNumberOfValues() != b.Coordinates.GetNumberOfValues())
  {
    return true;
  }

  for (vtkm::Id i = 0; i < a.Coordinates.GetNumberOfValues(); i++)
  {
    if (vtkm::cont::ArrayGetValue(i, a.Coordinates) !=
        vtkm::cont::ArrayGetValue(i, b.Coordinates))
    {
      return true;
    }
  }
  return false;
}

int DiySerialization(int argc, char** argv)
{
  vtkm::cont::Initialize(argc, argv);
  vtkmdiy::mpi::communicator comm;
  vtkm::cont::EnvironmentTracker::SetCommunicator(comm);
  vtkmdiy::Master master(comm);

  auto nblocks = comm.size();
  std::vector<int> gids;

  vtkmdiy::RoundRobinAssigner assigner(comm.size(), nblocks);
  assigner.local_gids(comm.rank(), gids);

  // In our example nblocks == num_ranks, thus gids.size() == 1
  auto gid = gids[0];

  // The link will be eventually freed by DIY.
  auto link = new vtkmdiy::Link;

  // Connect each blocks with themself.
  vtkmdiy::BlockID neighbor;
  neighbor.gid = gid;
  neighbor.proc = assigner.rank(neighbor.gid);
  link->add_neighbor(neighbor);

  compute(master, link, gid);

  return EXIT_SUCCESS;
}
