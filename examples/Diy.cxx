//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//============================================================================

#include <vtkm/cont/Algorithm.h>
#include <vtkm/cont/ArrayCopy.h>
#include <vtkm/cont/ArrayGetValues.h>
#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/DIYMemoryManagement.h>
#include <vtkm/cont/EnvironmentTracker.h>
#include <vtkm/cont/Initialize.h>
#include <vtkm/thirdparty/diy/diy.h>

#include <memory>

int Diy(int argc, char** argv)
{
  vtkm::cont::Initialize(argc, argv);

  //// BEGIN-EXAMPLE DIYSetupComm.cxx
  vtkmdiy::mpi::communicator comm;
  vtkm::cont::EnvironmentTracker::SetCommunicator(comm);

  auto nblocks = comm.size();
  std::vector<int> gids;

  vtkmdiy::RoundRobinAssigner assigner(comm.size(), nblocks);
  assigner.local_gids(comm.rank(), gids);

  // In our example nblocks == num_ranks, thus gids.size() == 1
  auto gid = gids[0];

  // The link will be eventually freed by DIY.
  auto link = new vtkmdiy::Link;

  // Connect each blocks with itself.
  vtkmdiy::BlockID neighbor;
  neighbor.gid = gid;
  neighbor.proc = assigner.rank(neighbor.gid);
  link->add_neighbor(neighbor);
  //// END-EXAMPLE DIYSetupComm.cxx

  vtkm::cont::ArrayHandle<vtkm::Int32> inputArrayHandle;
  vtkm::cont::ArrayCopy(vtkm::cont::ArrayHandleIndex{ 1000 }, inputArrayHandle);

  //// BEGIN-EXAMPLE DIYForeach.cxx
  vtkmdiy::Master master(comm);

  struct MyBlock
  {
    vtkm::cont::ArrayHandle<vtkm::Int32> in;
  };

  MyBlock block{ inputArrayHandle };
  master.add(gid, &block, link);

  master.foreach (
    [&](MyBlock* b, const vtkmdiy::Master::ProxyWithLink& cp)
    {
      vtkm::cont::Algorithm::Sort(b->in);
      cp.enqueue(cp.link()->target(0), b->in);
    });
  //// LABEL DIYMasterExchange
  vtkm::cont::DIYMasterExchange(master);
  master.foreach (
    [&](MyBlock* b, const vtkmdiy::Master::ProxyWithLink& cp)
    {
      cp.dequeue(cp.link()->target(0).gid, b->in);

      auto median_idx = (b->in.GetNumberOfValues() / 2) - 1;
      auto median = vtkm::cont::ArrayGetValue(median_idx, b->in);

      cp.all_reduce(median, vtkmdiy::mpi::maximum<vtkm::Int32>());
    });
  vtkm::cont::DIYMasterExchange(master);

  if (comm.rank() == 0)
  {
    std::cout << "Max(median): "
              << master.proxy(master.loaded_block()).get<vtkm::Int32>() << std::endl;
  }
  //// END-EXAMPLE DIYForeach.cxx

  return EXIT_SUCCESS;
}
