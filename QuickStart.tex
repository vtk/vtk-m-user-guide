% -*- latex -*-

\chapter{Quick Start}
\label{chap:QuickStart}

In this chapter we go through the steps to create a simple program that uses \VTKm.
This ``hello world'' example presents only the bare minimum of features available.
The remainder of this book documents dives into much greater detail.

We will call the example program we are building \textfilename{VTKmQuickStart}.
It will demonstrate reading data from a file, processing the data with a filter, and rendering an image of the data.
Readers who are less interested in an explanation and are more interested in browsing some code can skip to Section \ref{sec:QuickStart:FullExample} on page \pageref{sec:QuickStart:FullExample}.


\section{Initialize}
\label{sec:QuickStart:Initialize}

\index{initialization|(}

The first step to using \VTKm is to initialize the library.
Although initializing \VTKm is \emph{optional}, it is recommend to allow \VTKm to configure devices and logging.
Initialization is done by calling the \vtkmcont{Initialize} function.
The \textidentifier{Initialize} function is defined in the \vtkmheader{vtkm/cont}{Initialize.h} header file.

\textidentifier{Initialize} takes the \textcode{argc} and \textcode{argv} arguments that are passed to the \textcode{main} function of your program, find any command line arguments relevant to \VTKm, and remove them from the list to make further command line argument processing easier.

\vtkmlisting{Initializing \VTKm.}{VTKmQuickStartInitialize.cxx}

\textidentifier{Initialize} has many options to customize command line argument processing.
See Chapter \ref{chap:Initialization} for more details.

\begin{didyouknow}
  Don't have access to \textcode{argc} and \textcode{argv}?
  No problem.
  You can call \vtkmcont{Initialize} with no arguments.
\end{didyouknow}

\index{initialization|)}


\section{Reading a File}
\label{sec:QuickStart:ReadFile}

\index{file I/O!read|(}
\index{read file|(}

\VTKm comes with a simple I/O library that can read and write files in VTK legacy format.
These files have a ``\textfilename{.vtk}'' extension.

VTK legacy files can be read using the \vtkmio{VTKDataSetReader} object, which is declared in the \vtkmheader{vtkm/io}{VTKDataSetReader.h} header file.
The object is constructed with a string specifying the filename (which for this example we will get from the command line).
The data is then read in by calling the \classmember{VTKDataSetReader}{ReadDataSet} method.

\vtkmlisting{Reading data from a VTK legacy file.}{VTKmQuickStartReadFile.cxx}

The \classmember*{VTKDataSetReader}{ReadDataSet} method returns the data in a \vtkmcont{DataSet} object.
The structure and features of a \textidentifier{DataSet} object is described in Chapter \ref{chap:DataSet}.
For the purposes of this quick start, we will treat \textidentifier{DataSet} as a mostly opaque object that gets passed to and from operations in \VTKm.

More information about \VTKm's file readers and writers can be found in Chapter \ref{chap:FileIO}.

\index{read file|)}
\index{file I/O!read|)}


\section{Running a Filter}
\label{sec:QuickStart:Filter}

\index{filter|(}

Algorithms in \VTKm are encapsulated in units called \keyterm{filters}.
A filter takes in a \textidentifier{DataSet}, processes it, and returns a new \textidentifier{DataSet}.
The returned \textidentifier{DataSet} often, but not always, contains data inherited from the source data.

\VTKm comes with many filters, which are documented in Chapter \ref{chap:RunningFilters}.
For this example, we will demonstrate the use of the \vtkmfilter{MeshQuality} filter, which is defined in the \vtkmheader{vtkm/filter}{MeshQuality.h} header file.
The \textidentifier{MeshQuality} filter will compute for each cell in the input data will compute a quantity representing some metric of the cell's shape.
Several metrics are available, and in this example we will find the area of each cell.

Like all filters, \textidentifier{MeshQuality} contains an \classmember*{MeshQuality}{Execute} method that takes an input \textidentifier{DataSet} and produces an output \textidentifier{DataSet}.
It also has several methods used to set up the parameters of the execution.
Section \ref{sec:MeshQuality} provides details on all the options of \textidentifier{MeshQuality}.
Suffice it to say that in this example we instruct the filter to find the area of each cell, which it will output to a field named ``area.''

\vtkmlisting{Running a filter.}{VTKmQuickStartFilter.cxx}

\index{filter|)}


\section{Rendering an Image}
\label{sec:QuickStart:Render}

\index{rendering|(}

Although it is possible to leverage external rendering systems, \VTKm comes with its own self-contained image rendering algorithms.
These rendering classes are completely implemented with the parallel features provided by \VTKm, so using rendering in \VTKm does not require any complex library dependencies.

Even a simple rendering scene requires setting up several parameters to establish what is to be featured in the image including what data should be rendered, how that data should be represented, where objects should be placed in space, and the qualities of the image to generate.
Consequently, setting up rendering in \VTKm involves many steps.
Chapter \ref{chap:Rendering} goes into much detail on the ways in which a rendering scene is specified.
For now, we just briefly present some boilerplate to achieve a simple rendering.

\vtkmlisting[ex:VTKmQuickStartRender]{Rendering data.}{VTKmQuickStartRender.cxx}

The first step in setting up a render is to create a \index{scene}\keyterm{scene}.
A scene comprises some number of \index{actor}\keyterm{actors}, which represent some data to be rendered in some location in space.
In our case we only have one \textidentifier{DataSet} to render, so we simply create a single actor and add it to a scene as shown in lines \ref{ex:VTKmQuickStartRender.cxx:scene-start}--\ref{ex:VTKmQuickStartRender.cxx:scene-end}.

The second step in setting up a render is to create a \index{view}\keyterm{view}.
The view comprises the aforementioned scene, a \index{mapper}\keyterm{mapper}, which describes how the data are to be rendered, and a \index{canvas}\keyterm{canvas}, which holds the image buffer and other rendering context.
The view is created in line \ref{ex:VTKmQuickStartRender.cxx:view}.
The image generation is then performed by calling \classmember*{View}{Paint} on the view object (line \ref{ex:VTKmQuickStartRender.cxx:paint}).
However, the rendering done by \VTKm's rendering classes is performed offscreen, which means that the result does not appear on your computer's monitor.
The easiest way to see the image is to save it to an image file using the \classmember*{View}{SaveAs} method (line \ref{ex:VTKmQuickStartRender.cxx:save}).

\index{rendering|)}


\section{The Full Example}
\label{sec:QuickStart:FullExample}

Putting together the examples from Sections \ref{sec:QuickStart:Initialize} to \ref{sec:QuickStart:Render}, here is a complete program for reading, processing, and rendering data with \VTKm.

\vtkmlisting[ex:QuickStartFull]{Simple example of using \VTKm.}{VTKmQuickStart.cxx}


\section{Build Configuration}
\label{sec:QuickStart:Build}

Now that we have the program listed in Example \ref{ex:QuickStartFull}, we still need to compile it with the appropriate compilers and flags.
By far the easiest way to compile \VTKm code is to use CMake.
CMake commands that can be used to link code to \VTKm are discussed in Section \ref{sec:LinkingToVTKm}.
The following example provides a minimal \index{CMakeLists.txt}\textfilename{CMakeLists.txt} required to build this program.

\lstinputlisting[language={}, caption={\textfilename{CMakeLists.txt} to build a program using \VTKm.}]{examples/VTKmQuickStart.cmake}

The first two lines contain boilerplate for any \textfilename{CMakeLists.txt} file.
They all should declare the minimum CMake version required (for backward compatibility) and have a \textcode{project} command to declare which languages are used.

The remainder of the commands find the \VTKm library, declare the program begin compiled, and link the program to the \VTKm library.
These steps are described in detail in Section \ref{sec:LinkingToVTKm}.
